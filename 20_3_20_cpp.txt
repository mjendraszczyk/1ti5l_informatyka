Program do obliczenia wyroznika trojmanianu kwadratowego

#Założenia:
1)Na podstawie podanych współczynników f(x) wyznaczamy wyróżnik trójmianu kwadratowego oraz miejsca zerowe funkcji.

2) Na początek definiujemy zmienne 
a
b
c

3) Pytamy się użytkownika o wartość zmiennych i zapisujemy je do pamięci komputera.

4) Wyznaczamy wynik który również przechowujemy jako zmienna wg wzoru
wynik = (-b)^2-(4*a*c) 
Potęgowanie zróbcie funkcją pow()
5) Wyświetlamy użytkownikowi wartość współczynika trójmianu kwadratowego (delty)

6) Po wykonaniu wyniku zróbcie instrukcje warunkowe
    a) Jeżeli wynik == 0
        Ma wyświetlić że funkcja ma jedno miejsce zerowe
    b) Jeżeli wynik > 0 
        Ma wyświetlić że funkcja ma 2 miejsce zerowe
    c) Jeżeli wynik < 0 
        Ma wyświetlić że funkcja nie ma miejsc zerowych
        
7) Kończymy pracę programu.

#Wynik: 
Oczekuję w repozytorium:
    pliku .cpp
    skomilowanego pliku .exe
    
W razie problemów z połączeniem z repo dajcie znać wtedy omówie to przez ms teams.

Odradzam szukania gotowca
