
Witam,
Prosiłbym te osoby które nie przesłały dotąd zadań o jakiś kontakt w tej sprawie z informacją kiedy by mogły je optymistycznie podesłać.

Aktualnie do wykonania byłaby również dość prosta implementacja algorytmu euklidesa,
Czyli krótko mówiąc najwyższego wspólnego dzielnika dla pary liczb.

Założenia są proste:
Definiujecie zmienna a i b, które reprezentują podawane przez użytkownika liczby,
Następnie algorytm sprawdza czy liczby te które podaliśmy są takie same czy nie, jeśli są takie same to wynikiem będzie którakolwiek z podanych przez użytkownika liczb.
Tu generalnie można zrobić patent, gdzie w pętli sprawdzamy dopóki liczba a jest różna od b, to porównujemy je poniżej:

Natomiast jeśli liczby są różne to sprawdzamy która z nich jest większa.

Jeśli większa jest liczba a (tj pierwsza którą podał) to wykonujemy operację a=a-b;

Natomiast jeśli b to wykonujemy operację b = b-a;
Operacje są po to by sprowadzać je do wspólnej wartości na podstawie której mamy informację o największym wspólnym dzielniku.

W razie pytań, wątpliwości proszę o feedback(albo na dzienniku, fb albo hangoucie) , od części osób miałem oczywiście jakieś zapytania jednak wolałbym żeby w miarę możliwości każdy był w stanie zrealizować zadania :)

Pozdrawiam,
MJ
